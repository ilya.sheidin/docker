#specify default image
FROM node:alpine
#setup working directory
RUN mkdir /usr/app 
WORKDIR /usr/app
#setup dependencies 
RUN npm install -g @angular/cli
COPY ./ ./
RUN npm install


#default commands
CMD ["ng","serve"]
